from regardias.api.model_api import ModelAPI
from regardias.api.model_api import ModelAPIError


def delete_model(api_dict: dict, model_name: str):
    """Delete a model from a Regards instance/project"""
    api = ModelAPI(**api_dict)
    for m in model_name:
        try:
            api.delete_by_name(m)
        except ModelAPIError as e:
            print(f"{m} could not be deleted : {e}")


def export_model(api_dict: dict, model_name: str, all_models: bool):
    """Export a model from a Regards instance/project to a XML file"""
    api = ModelAPI(**api_dict)
    if all_models:
        models = api.get_list()
        model_name = [m.name for m in models]
    for m in model_name:
        try:
            api.export_to_xml(m)
        except ModelAPIError as e:
            print(f"{m} could not be exported: {e}")


def ingest_model(api_dict: dict, model_filename: str):
    """Ingest a model from a Regards instance/project from a XML file"""
    api = ModelAPI(**api_dict)
    for m in model_filename:
        try:
            api.import_from_xml(m)
        except ModelAPIError as e:
            print(f"{m} could not be ingested: {e}")
