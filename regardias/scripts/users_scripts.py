from regardias.api.users_api import UsersAPI


def add_user_group_access(api_dict: dict, email: str, group: str):
    """Add an access group for a user given by its email"""
    api = UsersAPI(**api_dict)
    api.add_access_group_for_user_by_email(email, group)


def remove_user_group_access(api_dict: dict, email: str, group: str):
    """Remove an access group for a user given by its email"""
    api = UsersAPI(**api_dict)
    api.remove_access_group_for_user_by_email(email, group)


def set_user_group_access(api_dict: dict, email: str, groups: str):
    """Set the access groups for a user given by its email"""
    api = UsersAPI(**api_dict)
    api.set_access_groups_for_user_by_email(email, list(groups))
