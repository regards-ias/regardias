import click

from regardias.scripts.model_scripts import delete_model
from regardias.scripts.model_scripts import export_model
from regardias.scripts.model_scripts import ingest_model


@click.group(help="A collection of models related tools")
@click.pass_context
def model(ctx):
    pass  # This is a group method. See the methods belonging to the group for actual implementation.


@click.command(no_args_is_help=True)
@click.pass_context
@click.option("-m", "--model_name", type=str, multiple=True)
def delete_model_cli(ctx, model_name: str):
    """Delete a model from a Regards instance/project"""
    delete_model(ctx.obj["API"], model_name)


@click.command(no_args_is_help=True)
@click.pass_context
@click.option(
    "-m", "--model_name", "model_names", type=click.Path(exists=True), multiple=True
)
@click.option("--all", "all_models", is_flag=True, help="Export all models")
def export_model_cli(ctx, model_name: str, all_models: bool):
    """Export a model from a Regards instance/project to a XML file"""
    export_model(ctx.obj["API"], model_name, all_models)


@click.command(no_args_is_help=True)
@click.pass_context
@click.option("-m", "--model_filename", type=click.Path(exists=True), multiple=True)
def ingest_model_cli(ctx, model_filename: str):
    """Ingest a model from a Regards instance/project from a XML file"""
    ingest_model(ctx.obj["API"], model_filename)


model.add_command(delete_model_cli, "delete")
model.add_command(export_model_cli, "export")
model.add_command(ingest_model_cli, "ingest")


if __name__ == "__main__":
    export_model_cli()
