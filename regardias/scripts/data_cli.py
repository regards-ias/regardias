from typing import List

import click

from regardias.scripts.data_scripts import copy_files
from regardias.scripts.data_scripts import list_files


@click.group(help="A collection of tools to deal with the data")
def data():
    pass  # This is a group method. See the methods belonging to the group for actual implementation.


@click.command(no_args_is_help=True)
@click.option("-p", "--prefix_path", required=True, type=click.Path(exists=True))
@click.option(
    "-f",
    "--filename-template",
    "filename_template",
    required=True,
    multiple=True,
    type=str,
)
@click.option("-o", "--output-file", "output_file", required=True, type=str)
def cli_list_files(prefix_path: str, filename_template: List[str], output_file: str):
    """List data files of interest"""
    list_files(prefix_path, filename_template, output_file)


@click.command(no_args_is_help=True)
@click.option(
    "-d", "--data-list", "data_list", required=True, type=click.Path(exists=True)
)
@click.option(
    "-t", "--thumbnails-dir", "thumbnails_dir", required=True, type=click.Path()
)
@click.option("-o", "--output-dir", "output_dir", required=True, type=click.Path())
@click.option("--dry-run", "dry_run", is_flag=True)
@click.option("--force", "force", is_flag=True)
def cli_copy_files(
    data_list: str, output_dir: str, dry_run: bool, force: bool, thumbnails_dir: str
):
    """Copy files to a new directory

    At the moment, the filesystem where the JWST data reduction is performed is not compatible with REGARDS.
    It is necessary to move data from /data/cluster/JWST/RealData/... to /data/ceph-data/JWST/REGARDS_DATA/"""
    copy_files(data_list, output_dir, dry_run, force, thumbnails_dir)


data.add_command(cli_list_files, "list-files")
data.add_command(cli_copy_files, "copy-files")
