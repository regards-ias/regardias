import click

from regardias.scripts.project_scripts import backup_full_config
from regardias.scripts.project_scripts import export_full_config
from regardias.scripts.project_scripts import export_microservice_config
from regardias.scripts.project_scripts import import_full_config
from regardias.scripts.project_scripts import import_microservice_config
from regardias.scripts.project_scripts import purge_project
from regardias.tools import create_archive
from regardias.tools import load_project_environment

MICROSERVICES = ["rs-access-project", "rs-catalog", "rs-dam", "rs-ingest", "rs-storage"]


@click.group(help="A collection of tools to deal with the projects administration")
@click.pass_context
def project(ctx):
    pass  # This is a group method. See the methods belonging to the group for actual implementation.


@click.command()
@click.pass_context
def purge_project_cli(ctx):
    """Delete datasources, datasets, data models and attributes from the project"""
    purge_project(ctx.obj["API"])


@click.command(no_args_is_help=True)
@click.pass_context
@click.option("-m", "--microservice", type=str)
@click.option("-f", "--filename", "output_filename", type=str)
@click.option("--force", is_flag=True)
def export_microservice_config_cli(
    ctx,
    microservice: str,
    output_filename: str,
    force: bool,
):
    """Export microservice configuration as JSON file"""
    export_microservice_config(ctx.obj["API"], microservice, output_filename, force)


@click.command()
@click.pass_context
@click.option("-o", "--output_filename", type=str)
def export_full_config_cli(ctx, output_filename):
    """Export project configuration as collection of JSON files in a .tar.gz archive"""
    export_full_config(ctx.obj["API"], output_filename)


@click.command(no_args_is_help=True)
@click.pass_context
@click.option("-m", "--microservice", type=str)
@click.option("-f", "--filename", "input_filename", type=click.Path(exists=True))
def import_microservice_config_cli(
    ctx,
    microservice: str,
    input_filename: str,
):
    """Import microservice configuration from JSON file"""
    import_microservice_config(ctx.obj["API"], microservice, input_filename)


@click.command(no_args_is_help=True)
@click.pass_context
@click.option("-f", "--filename", "input_filename", type=click.Path(exists=True))
def import_full_config_cli(ctx, input_filename: str):
    """Import project configuration from a collection of JSON files in a .tar.gz archive"""
    import_full_config(ctx.obj["API"], input_filename)


@click.command()
@click.pass_context
@click.option("-r", "--gitlab", "gitlab_repository", type=str)
def backup_full_config_cli(ctx, gitlab_repository):
    """Backup project configuration to a Gitlab repository"""
    backup_full_config(ctx.obj["API"], gitlab_repository)


project.add_command(backup_full_config_cli, "backup-full-config")
project.add_command(purge_project_cli, "purge")
project.add_command(export_microservice_config_cli, "export-microservice-config")
project.add_command(export_full_config_cli, "export-full-config")
project.add_command(import_microservice_config_cli, "import-microservice-config")
project.add_command(import_full_config_cli, "import-full-config")
