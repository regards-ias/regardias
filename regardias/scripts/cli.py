import click

from regardias.scripts.attribute_cli import attribute
from regardias.scripts.data_cli import data
from regardias.scripts.dataset_cli import dataset
from regardias.scripts.model_cli import model
from regardias.scripts.project_cli import project
from regardias.scripts.sip_cli import sip
from regardias.scripts.users_cli import users
from regardias.tools import load_project_environment


@click.group()
@click.option(
    "-e",
    "--environment",
    type=click.Path(exists=True),
    help="Use values defined in file to replace other options.",
)
@click.option(
    "-i",
    "--instance-url",
    "instance_url",
    type=str,
    help="e.g. https://regards-pp.ias.u-psud.fr/",
)
@click.option("-n", "--project-name", "project_name", type=str, help="e.g. jwst")
@click.option("-u", "--username", type=str)
@click.option("-p", "--password", type=str)
@click.pass_context
def regardias(ctx, environment, instance_url, project_name, username, password):
    if environment:
        instance_url, password, project_name, username = load_project_environment(
            environment
        )
    ctx.ensure_object(dict)
    ctx.obj["API"] = {
        "instance_url": instance_url,
        "project_name": project_name,
        "username": username,
        "password": password,
    }


regardias.add_command(attribute)
regardias.add_command(data)
regardias.add_command(dataset)
regardias.add_command(model)
regardias.add_command(project)
regardias.add_command(sip)
regardias.add_command(users)
