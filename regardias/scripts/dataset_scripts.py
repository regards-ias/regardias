from typing import List

from regardias.api.dataset_api import DatasetAPI
from regardias.api.dataset_api import DatasetAPIError


def list_datasets(api_dict: dict):
    """List datasets from a Regards instance/project"""
    api = DatasetAPI(**api_dict)
    datasets = api.list_available_datasets()
    print(f"Available datasets for {api.instance_url} ({api.project_name}):")
    for dataset in datasets:
        print(dataset)


def delete_datasets(api_dict: dict, dataset_names: List[str], all_datasets: bool):
    """Delete datasets from a Regards instance/project"""
    api = DatasetAPI(**api_dict)
    if all_datasets:
        api.delete_all()
    else:
        for name in dataset_names:
            try:
                api.delete_by_name(name)
            except DatasetAPIError as e:
                print(f"Dataset {name} could not be deleted : {e}")
