from pathlib import Path
from typing import List


def list_files(prefix_path: str, filename_template: List[str], output_file: str):
    """List data files of interest"""
    prefix_path = Path(prefix_path)
    with open(output_file, "w") as f:
        for template in filename_template:
            for filename in prefix_path.glob(template):
                f.write(f"{filename.resolve().as_posix()}\n")


def copy_files(
    data_list: str, output_dir: str, dry_run: bool, force: bool, thumbnails_dir: str
):
    """Copy files to a new directory

    At the moment, the filesystem where the JWST data reduction is performed is not compatible with REGARDS.
    It is necessary to move data from /data/cluster/JWST/RealData/... to /data/ceph-data/JWST/REGARDS_DATA/"""
    output_dir = Path(output_dir)
    with open(data_list, "r") as f:
        filenames = f.readlines()
    for filename in filenames:
        filename = Path(filename.strip())
        new_filename = output_dir / filename.name
        print(f"Copy {filename} to {new_filename}")
        template = "(.*)IASv(.*).fits"
        _, product_version = re.findall(template, filename.name)[0]
        # Do not copy if dry-run, or if the file already exists and --force is not passed
        if not dry_run and (not new_filename.exists() or force):
            shutil.copy2(str(filename), str(new_filename))
            thumbnail_filename = (
                Path(thumbnails_dir) / filename.with_suffix(".png").name
            )
            if thumbnail_filename.exists():
                shutil.copy2(
                    str(thumbnail_filename), str(new_filename.with_suffix(".png"))
                )
            try:
                readme_filename = list(
                    filename.parent.glob(f"README_*IASv{product_version}.txt")
                )[0]
                shutil.copy2(
                    str(filename.parent / readme_filename),
                    str(
                        new_filename.with_stem(
                            f"README_{new_filename.stem}"
                        ).with_suffix(".txt")
                    ),
                )
            except IndexError:
                print("No README file available for this product")
