from pathlib import Path

from regardias.api.sip_api import SIPAPI
from regardias.sip_manager.sip import SIPError
from regardias.sip_manager.sip_factory import SIPFactory


def generate_sip_from_fits(template: str, data: str, output: str, project: str):
    """Create a SIP file from a template and the metadata contained in a FITS file"""
    sip = SIPFactory.from_template(template, project)
    sip.update_from_fits_file(data)
    sip.to_json_file(output)


def generate_multiple_sips_from_fits(
    template: str, input_file: str, output_directory: str, overwrite: bool, project: str
):
    """Generate SIPs from FITS files.

    The FITS files should be provided through a text file containing the list of files to be used."""
    with open(input_file, "r") as f:
        filenames = f.readlines()
    for filename in filenames:
        output = Path(output_directory) / Path(filename).with_suffix(".json").name
        if output.exists() and not overwrite:
            print(f"SIP for {filename} already exists, skip it")
        else:
            print(f"Create SIP for {filename}")
            filename = filename.rstrip("\n")
            sip = SIPFactory.from_template(template, project)
            try:
                sip.update_from_fits_file(filename)
                output = (
                    Path(output_directory) / Path(filename).with_suffix(".json").name
                )
                sip.to_json_file(output)
            except SIPError:
                print(f"The SIP could not be created for {filename}")


def ingest_sip(
    api_dict: dict,
    filename: str,
    from_directory: bool = False,
    from_filelist: bool = False,
):
    """Ingest SIP to a Regards instance/project"""
    api = SIPAPI(**api_dict)
    if from_directory:
        api.ingest_multiple_sips_from_directory(filename)
    elif from_filelist:
        api.ingest_multiple_sips_from_filelist(filename)
    else:
        api.ingest_sip(filename)


def ingest_sips_from_directory(api_dict: dict, directory: str):
    """Ingest all SIPs contained in a directory to a Regards instance/project"""
    api = SIPAPI(**api_dict)
    api.ingest_multiple_sips_from_directory(directory)


def ingest_sips_from_filelist(api_dict: dict, filelist: str):
    """Ingest all SIPs contained in a text file list to a Regards instance/project"""
    api = SIPAPI(**api_dict)
    api.ingest_multiple_sips_from_filelist(filelist)
