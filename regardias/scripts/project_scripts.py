import datetime
import shutil
import tarfile
from pathlib import Path

from git import Repo

from regardias.api.api import API
from regardias.api.api import APIError
from regardias.api.attribute_api import AttributeAPI
from regardias.api.dataset_api import DatasetAPI
from regardias.api.datasource_api import DatasourceAPI
from regardias.api.model_api import ModelAPI
from regardias.api.model_api import ModelAPIError
from regardias.tools.files_tools import create_archive


def purge_project(api_dict: dict):
    """Delete datasources, datasets, data models and attributes from the project"""

    confirmation = input(
        "\n\n\n**********************************************\n"
        f"You are about to clean-up the full project {api_dict['project_name']} "
        f"from {api_dict['instance_url']}.\n\n"
        "DATASETS, MODELS, DATSOURCES WILL BE LOST.\n\n"
        "Are you sure you want yo proceed?"
    )
    if confirmation != "yes":
        print("\nNo? Good, take a moment to think again.")
        exit()

    for my_api in [DatasetAPI, DatasourceAPI, ModelAPI, AttributeAPI]:
        api = my_api(**api_dict)
        api.delete_all()


def export_microservice_config(
    api_dict: dict,
    microservice: str,
    output_filename: str,
    force: bool,
):
    """Export microservice configuration as JSON file"""
    if output_filename is None:
        output_filename = f"config-{microservice}.json"
    if Path(output_filename).exists() and not force:
        print(
            f"{output_filename} already exists, if you want to overwrite it, use the -f (--force) option"
        )
    else:
        api = API(**api_dict)
        api.export_microservice_configuration(microservice, output_filename)


def export_full_config(api_dict: dict, output_filename: str):
    """Export project configuration as collection of JSON files in a .tar.gz archive"""
    p = Path.cwd() / "tmp"
    p.mkdir()
    api = API(**api_dict)
    microservices = [
        "rs-access-project",
        "rs-catalog",
        "rs-dam",
        "rs-ingest",
        "rs-storage",
    ]
    for microservice in microservices:
        try:
            api.export_microservice_configuration(
                microservice, p / f"config-{microservice}.json"
            )
        except APIError:
            print(f"Unable to retrieve configuration for {microservice}")
    model_api = ModelAPI(**api_dict)
    for model in model_api.get_list():
        model_api.export_to_xml(model.name, p / f"{model.name}.xml")
    instance_name = api_dict["instance_url"].removeprefix("https://").removesuffix("/")
    project_name = api_dict["project_name"]
    if not output_filename:
        output_filename = f"config_{instance_name}_{project_name}_{datetime.datetime.now():%Y%m%d}.tar.gz"
    files = list(p.glob("*.[json xml]*"))
    create_archive(files, output_filename, delete_files=True)
    p.rmdir()


def import_microservice_config(
    api_dict: dict,
    microservice: str,
    input_filename: str,
):
    """Import microservice configuration from JSON file"""
    api = API(**api_dict)
    api.import_microservice_configuration(microservice, input_filename)


def import_full_config(api_dict, input_filename: str):
    """Import project configuration from a collection of JSON files in a .tar.gz archive"""
    instance_url = api_dict["instance_url"]
    project_name = api_dict["project_name"]
    confirmation = input(
        "\n\n\n**********************************************\n"
        f"You are about to import the full configuration:\n"
        f"- from {input_filename}\n"
        f"- into {instance_url}\n"
        f"- for project {project_name}.\n\n"
        "OLD CONFIGURATION WILL BE ERASED.\n\n"
        "Are you sure you want yo proceed?"
    )
    if confirmation != "yes":
        print("\nNo? Good, take a moment to think again.")
        exit()

    p = Path.cwd() / "tmp"
    p.mkdir()
    with tarfile.open(input_filename, "r") as tar:
        tar.extractall(p)

    # Import models
    model_api = ModelAPI(**api_dict)
    for model_configuration_file in p.glob("*.xml"):
        model = model_configuration_file.name.removesuffix(".xml")
        try:
            model_api.import_from_xml(model_configuration_file)
        except ModelAPIError as e:
            print(f"Unable to ingest model {model}: {e}")
        model_configuration_file.unlink()

    # Import microservices
    api = API(**api_dict)
    microservices = [
        "rs-storage",
        "rs-ingest",
        "rs-dam",
        "rs-catalog",
        "rs-access-project",
    ]

    for microservice in microservices:
        microservice_configuration_file = p / f"config-{microservice}.json"
        try:
            api.import_microservice_configuration(
                microservice, microservice_configuration_file.resolve().as_posix()
            )
        except APIError as e:
            print(f"Unable to import configuration for {microservice}: {e}")
        microservice_configuration_file.unlink()

    p.rmdir()


def backup_full_config(api_dict: dict, gitlab_repository: str):
    """Backup project configuration to a Gitlab repository"""
    repository = Repo.clone_from(gitlab_repository, "tmp")
    p = Path.cwd() / "tmp"
    api = API(**api_dict)
    microservices = [
        "rs-access-project",
        "rs-catalog",
        "rs-dam",
        "rs-ingest",
        "rs-storage",
    ]
    for microservice in microservices:
        try:
            api.export_microservice_configuration(
                microservice, p / f"config-{microservice}.json"
            )
        except APIError:
            print(f"Unable to retrieve configuration for {microservice}")
    model_api = ModelAPI(**api_dict)
    for model in model_api.get_list():
        model_api.export_to_xml(model.name, p / f"{model.name}.xml")
    repository.index.add("*")
    today = datetime.date.today().isoformat()
    repository.index.commit(f"Backup {today}")
    repository.remote().push()
    shutil.rmtree(p)
