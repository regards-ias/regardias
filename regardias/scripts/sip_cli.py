import click

from regardias.scripts.sip_scripts import generate_multiple_sips_from_fits
from regardias.scripts.sip_scripts import generate_sip_from_fits
from regardias.scripts.sip_scripts import ingest_sip


@click.group(help="A collection of SIPs related tools")
@click.pass_context
def sip(ctx):
    pass  # This is a group method. See the methods belonging to the group for actual implementation.


@click.command(no_args_is_help=True)
@click.option("-t", "--template", required=True, type=click.Path(exists=True))
@click.option("-d", "--data", required=True, type=click.Path(exists=True))
@click.option("-o", "--output", required=True, type=click.Path())
@click.option(
    "-p", "--project", required=False, type=str, help="Available projects are: JWST"
)
def generate_sip_from_fits_cli(template: str, data: str, output: str, project: str):
    """Create a SIP file from a template and the metadata contained in a FITS file"""
    generate_sip_from_fits(template, data, output, project)


@click.command(no_args_is_help=True)
@click.option("-t", "--template", required=True, type=click.Path(exists=True))
@click.option(
    "-i", "--input-file", "input_file", required=True, type=click.Path(exists=True)
)
@click.option(
    "-o", "--output-directory", "output_directory", required=True, type=click.Path()
)
@click.option(
    "-p", "--project", required=False, type=str, help="Available projects are: JWST"
)
@click.option("--overwrite", "overwrite", is_flag=True)
def generate_multiple_sips_from_fits_cli(
    template: str, input_file: str, output_directory: str, overwrite: bool, project: str
):
    """Generate SIPs from FITS files.

    The FITS files should be provided through a text file containing the list of files to be used."""
    generate_multiple_sips_from_fits(
        template, input_file, output_directory, overwrite, project
    )


@click.command(no_args_is_help=True)
@click.pass_context
@click.argument("filename", type=click.Path(exists=True))
@click.option(
    "--from-directory",
    is_flag=True,
    help="If used, you must provide a directory containing the SIPs to ingest (all .json files will be considered as SIPs)",
)
@click.option(
    "--from-filelist",
    is_flag=True,
    help="If used, you must provide a file containing the paths to the SIPs to ingest",
)
def ingest_sip_cli(ctx, filename: str, from_directory, from_filelist):
    """Ingest SIP to a Regards instance/project"""
    if from_directory and from_filelist:
        raise click.BadOptionUsage(
            "You can only use one of the options --from-directory or --from-filelist"
        )
    ingest_sip(
        ctx.obj["API"],
        filename,
        from_directory=from_directory,
        from_filelist=from_filelist,
    )


sip.add_command(generate_sip_from_fits_cli, "generate-from-fits")
sip.add_command(generate_multiple_sips_from_fits_cli, "generate-multiple-from-fits")
sip.add_command(ingest_sip_cli, "ingest")
