import click

from regardias.api import UsersAPI
from regardias.scripts.users_scripts import add_user_group_access
from regardias.scripts.users_scripts import remove_user_group_access
from regardias.scripts.users_scripts import set_user_group_access


@click.group(help="A collection of tools to deal with the data")
@click.pass_context
def users(ctx):
    pass  # This is a group method. See the methods belonging to the group for actual implementation.


@click.command(no_args_is_help=True)
@click.pass_context
@click.option("-m", "--email", "email", type=str, help="Email of the user to modifiy")
@click.option("-g", "--group", "group", type=str, help="Access groups")
def add_user_group_access_cli(ctx, email, group):
    """Add an access group for a user given by its email"""
    add_user_group_access(ctx.obj["API"], email, group)


@click.command(no_args_is_help=True)
@click.pass_context
@click.option("-m", "--email", "email", type=str, help="Email of the user to modifiy")
@click.option("-g", "--group", "group", type=str, help="Access groups")
def remove_user_group_access_cli(ctx, email, group):
    """Remove an access group for a user given by its email"""
    remove_user_group_access(ctx.obj["API"], email, group)


@click.command(no_args_is_help=True)
@click.pass_context
@click.option("-m", "--email", "email", type=str, help="Email of the user to modifiy")
@click.option("-g", "--group", "groups", type=str, multiple=True, help="Access groups")
def set_user_group_access_cli(ctx, email, groups):
    """Set the access groups for a user given by its email"""
    set_user_group_access(ctx.obj["API"], email, groups)


users.add_command(add_user_group_access_cli, "add-access-group")
users.add_command(remove_user_group_access_cli, "remove-access-group")
users.add_command(set_user_group_access_cli, "set-access-groups")
