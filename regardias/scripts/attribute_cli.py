from typing import List

import click

from regardias.scripts.attribute_scripts import delete_attributes


@click.group(help="A collection of attributes related tools")
@click.pass_context
def attribute(ctx):
    pass  # This is a group method. See the methods belonging to the group for actual implementation.


@click.command(no_args_is_help=True)
@click.pass_context
@click.option(
    "-a",
    "--attribute_name",
    type=str,
    multiple=True,
    help="Can pass multiple attributes",
)
@click.option("--all", "all_attributes", is_flag=True, help="Delete all attributes")
def cli_delete(ctx, attribute_name: List[str], all_attributes: bool):
    """Delete attributes from a Regards instance/project"""
    delete_attributes(ctx.obj["API"], attribute_name, all_attributes)


attribute.add_command(cli_delete, "delete")
