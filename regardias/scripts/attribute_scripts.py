from typing import List

from regardias.api.attribute_api import AttributeAPI
from regardias.api.attribute_api import AttributeAPIError


def delete_attributes(
    api_dict: dict, attribute_name: List[str], all_attributes: bool = False
):
    """Delete attributes from a Regards instance/project"""
    api = AttributeAPI(**api_dict)
    if all_attributes:
        api.delete_all()
    else:
        for a in attribute_name:
            try:
                api.delete_by_name(a)
            except AttributeAPIError as e:
                print(f"{a} could not be deleted : {e}")
