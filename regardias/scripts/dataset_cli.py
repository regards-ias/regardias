from typing import List

import click

from regardias.scripts.dataset_scripts import delete_datasets
from regardias.scripts.dataset_scripts import list_datasets


@click.group(help="A collection of datasets related tools")
@click.pass_context
def dataset(ctx):
    pass  # This is a group method. See the methods belonging to the group for actual implementation.


@click.command()
@click.pass_context
def cli_list_datasets(ctx):
    """List all datasets from a Regards instance/project"""
    list_datasets(ctx.obj["API"])


@click.command(no_args_is_help=True)
@click.pass_context
@click.option(
    "-d", "--dataset_names", type=str, multiple=True, help="Can pass multiple datasets"
)
@click.option("--all", "all_datasets", is_flag=True, help="Delete all datasets")
def cli_delete_datasets(ctx, dataset_names: List[str], all_datasets: bool):
    """Delete datasets from a Regards instance/project"""
    delete_datasets(ctx.obj["API"], dataset_names, all_datasets)


dataset.add_command(cli_list_datasets, "list")
dataset.add_command(cli_delete_datasets, "delete")
