from pathlib import Path
from typing import Union

import matplotlib.pyplot as plt
from astropy.io import fits
from scipy.stats import scoreatpercentile


def create_thumbnail_from_fits(
    filename: Union[str, Path],
    prefix_path: Union[str, Path] = "/data/cluster/JWST/Thumbnails",
):
    """Create a stamp image of the data

    PNG image stamp at"""
    filename = Path(filename)
    prefix_path = Path(prefix_path)
    data = fits.getdata(filename.as_posix())
    if data.ndim == 3:
        data = data[0, ...]
    if data.ndim == 4:
        data = data[0, 0, ...]
    vmin, vmax = scoreatpercentile(data, [3, 97])
    fig, ax = plt.subplots(figsize=(4, 4))
    ax.imshow(data, vmin=vmin, vmax=vmax, cmap="inferno")
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    plt.savefig(prefix_path / filename.with_suffix(".png").name)
    plt.close()
