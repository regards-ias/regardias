import dotenv


def load_project_environment(environment):
    print("Environment file is provided, other options will be ignored.")
    config = dotenv.dotenv_values(environment)
    instance_url = config["INSTANCE-URL"]
    project_name = config["PROJECT-NAME"]
    username = config["USERNAME"]
    password = config["PASSWORD"]
    return instance_url, password, project_name, username
