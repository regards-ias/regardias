import hashlib
import os
import tarfile
from pathlib import Path
from typing import List
from typing import Union


def compute_md5sum(filename: Union[str, Path]):
    filename = Path(filename)
    if not filename.exists():
        raise FileNotFoundError(f"{filename} does not exist")
    return hashlib.md5(filename.read_bytes()).hexdigest()


def create_archive(files: List[Path], archive_filename: str, delete_files=False):
    with tarfile.open(archive_filename, "w:gz") as tar:
        for f in files:
            tar.add(f, arcname=f.name)
        tar.close()
    if delete_files:
        for f in files:
            os.remove(f)
