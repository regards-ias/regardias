import os
from pathlib import Path
from typing import Generator
from typing import List
from typing import TypeAlias
from typing import Union

PathLike: TypeAlias = Union[os.PathLike, str, Path]
PathLikeIterator: TypeAlias = Union[List[PathLike], Generator]
