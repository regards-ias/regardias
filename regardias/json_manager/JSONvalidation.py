class JSONvalidation:
    """
    this class allows to setup the validation the type of validation requested. Minimalistic validation has
    been implemented for the time being: none of recursive (checking array elements and object fields
    recursively). This case is subject to cha,ge once the the SIP package (and its validation capabilities) will
    be finalized
    """

    def __init__(self, recursive=False):
        """
        initialize a validation instance
        :param recursive: True is array items and object fields must be validated recursively
        """
        self.recursive = recursive
