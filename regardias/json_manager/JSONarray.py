from copy import deepcopy

from .JSONcollection import JSONcollection
from .JSONexception import JSONexception
from .JSONvalidation import JSONvalidation


class JSONarray(JSONcollection):
    """
    JSON element corresponding to an array (that is: list)
    """

    def __init__(self, data=None, validation=JSONvalidation(recursive=True)):
        """
        initialize an "in-memory" JSON array with a provided data
        :param data: data (None or list or JSONarray)
        :param validation: type of validation
        :exception JSONexception: data cannot be used to initialize a JSONarray
        """
        super().__init__(data, validation)

        # validation is performed on each array item
        if validation.recursive:
            self.validatedataitems(validation)

    def validatedata(self, data, validation=JSONvalidation()):
        """
        validate the data used for assignment (list, JSONarray or None expected)
        :param data: data to be validated
        :param validation: type of validation
        :exception JSONexception: data is not one of list, JSONarray or None
        """
        if not (isinstance(data, JSONarray) or isinstance(data, list) or data is None):
            raise JSONexception(f"cannot initialize JSONarray with {data}")

    def validatedataitems(self, validation=JSONvalidation()):
        """
        recursively validate all the data items of the array
        :param validation: type of validation
        :exception JSONexception: of of the array item is not JSON compatible
        """
        for i in self._field__value:
            self.validatedataitem(i, validation)

    def validatedataitem(self, value, validation=JSONvalidation()):
        """
        check that the array item is also JSON compatible (that is: there is nothing "nasty" deeply nested
        in the array elements
        :param value: the value we want to check
        :param validation: type of validation
        :exception JSONexception: the array item is not JSON compatible
        """
        json_type = self.getJSONtype(value)

        if issubclass(json_type, JSONarray):
            json_type(value).validatedataitems(validation)
        else:
            json_type(value, validation)

    def __iter__(self):
        """
        allows to iterate on each array element (that is: for i in array ...)
        :return: the current value
        """
        for value in self._field__value:
            yield value

    def __getitem__(self, key):
        """
        retrieve the value that corresponds to key (or the values is key is a slice)
        :param key: key of the object we want to retrieve (or slice ok keys)
        :return: the value that corresponds to key (or an array of values in key is a slice)
        """
        if isinstance(key, slice):
            indices = range(*key.indices(len(self._field__value)))
            return [self[i] for i in indices]

        return self.toJSON(self._field__value[key])

    def __setitem__(self, key, values):
        """
        set a new value to the item that corresponds to key (or a list of values if key is a slice)
        :param key: key of the object we want to update (or slice)
        :param value: new value of the item (or list of values if key is a slice)
        """
        if isinstance(key, slice):
            indices = range(*key.indices(len(self._field__value)))
            for i, j in enumerate(indices):
                self[j] = values[i]
        else:
            self._field__value[key] = self.toJSON(values)

    def __delitem__(self, key):
        """
        delete items that corresponds to key (or a list of values if key is a slice)
        :param key: key of the object we want to delete (or slice)
        :return: the current instance with deleted items
        """
        if isinstance(key, slice):
            indices = reversed(range(*key.indices(len(self._field__value))))
            for i in indices:
                del self._field__value[i]
        else:
            del self._field__value[key]

    def append(self, value):
        """
        append a value at the end of the array
        :param value: value we append at the end of the array
        :return: the updated array
        """
        self._field__value.append(value)
        return self

    def extend(self, values):
        """
        append a list of values at the end of the array
        :param values: values we append at the end of the array
        :return: the updated array
        """
        for value in values:
            self.append(value)
        return self

    def insert(self, pos, value):
        """
        insert a value at a given position of an array
        :param pos: position of the value insertion
        :param value: values we insert
        :return: the updated array
        """
        self._field__value.insert(pos, value)
        return self

    def __add__(self, value):
        """
        create a (deep) copy of the current instance and add a value at the end using the "+" operator
        :param value: value to be added
        :return: copy of the current instance with a new value added at the end
        """
        c = deepcopy(self)
        c.append(value)

        return c

    def __radd__(self, value):
        """
        create a (deep) copy of the current instance and add a value at the beginning using the "+" operator
        :param value: value to be added
        :return: copy of the current instance with a new value added at the beginning
        """
        c = deepcopy(self)
        c.insert(0, value)

        return c

    def __iadd__(self, value):
        """
        add a value at the end of the current instance using the "+=" operator
        :param value: value to be added
        :return: the current instance, with a new value added at the end
        """
        self.append(value)

        return self

    def count(self, value):
        """
        return the number of times value appears in the array:
        :param value: value we are looking for
        :return: number of times value appears in the array
        """
        return self._field__valuedata.count(value)

    def index(self, value, i=None, j=None):
        """
        return the index of the first occurrence of value in the array:
        :param value: value we are looking for
        :param i: first index to consider when searching. Ignored if None
        :param j: first index not to consider when searching. Ignored if None or if i is None
        :return: index of the first occurrence of value in the array
        :exception ValueError: the value has not been found
        """
        if i is None:
            return self._field__value.index(value)
        elif j is None:
            return self._field__value.index(value, i)
        else:
            return self._field__value.index(value, i, j)

    def reverse(self):
        """
        reverse the elements of the current instance
        :return: the current instance with reversed elements
        """
        return self._field__value.reverse()

    def pop(self, index=-1):
        """
        remove the item at the given index from the array and returns the removed item
        :param index:  index of the item we want to pop
        :return: the index we have retrieved
        :exception IndexError: index is out of range
        """
        return self.toJSON(self._field__value.pop(index, index))

    def remove(self, value):
        """
        remove the first matching element (which is passed as an argument) from the array
        :param value: the value we are looking for
        :exception ValueError: value is not in the array
        """
        self._field__value.remove(value)

    def sort(self, reverse=False, key=None):
        """
        sorts the items of a array in ascending or descending order
        :param reverse: if True, the sorted list is reversed (or sorted in Descending order)
        :param key: function that serves as a key for the sort comparison
        """
        self._field__value.sort(reverse=reverse, key=key)

    @classmethod
    def getstaticdefault(cls):
        """
        return a default "in-memory" JSONarray
        :return: []
        """
        return []
