from .JSONelement import JSONelement
from .JSONexception import JSONexception
from .JSONvalidation import JSONvalidation


class JSONnumber(JSONelement):
    """
    JSON element corresponding to a number (either int or float)
    """

    def __init__(self, data=None, validation=JSONvalidation()):
        """
        initialize an "in-memory" JSON number
        :param data: only numbers, JSONnumber or None expected (any other value raises an exception)
        :param validation: type of validation
        :exception JSONexception: data is not one of number, JSONnumber or None
        """
        super().__init__(data, validation)

    def validatedata(self, data, validation=JSONvalidation()):
        """
        validate the data used for assignment (number, JSONnumber or None expected)
        :param data: data to be validated
        :param validation: type of validation
        :exception JSONexception: data is not one of number, JSONnumber or None
        """
        if not (
            isinstance(data, JSONnumber)
            or isinstance(data, (int, float))
            or data is None
        ):
            raise JSONexception(f"cannot initialize JSONnumber with {data}")

    @classmethod
    def getdefaultelement(cls):
        """
        return a default "in-memory" JSONnumber
        :return: ""
        """
        return 0
