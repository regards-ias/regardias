from .JSONelement import JSONelement
from .JSONexception import JSONexception
from .JSONvalidation import JSONvalidation


class JSONstring(JSONelement):
    """
    JSON element corresponding to a string
    """

    def __init__(self, data="", validation=JSONvalidation()):
        """
        initialize an "in-memory" JSON string
        :param data: only strings, JSONstring or None expected (any other value raises an exception)
        :param validation: type of validation
        :exception JSONexception: data is not one of string, JSONstring or None
        """
        super().__init__(data, validation)

    def validatedata(self, data, validation=JSONvalidation()):
        """
        validate the data used for assignment (string, JSONstring or None expected)
        :param data: data to be validated
        :param validation: type of validation
        :exception JSONexception: data is not one of string, JSONstring or None
        """
        if not (isinstance(data, JSONstring) or isinstance(data, str) or data is None):
            raise JSONexception(f"cannot initialize a JSONstring with {data}")

    @classmethod
    def getdefaultelement(cls):
        """
        return a default "in-memory" JSONstring
        :return: ""
        """
        return ""
