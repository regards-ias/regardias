from importlib import import_module
from inspect import isclass

"""
The JSONmanager package is mainly a collection of modules, each one containing a single class. In order to ease the
way we import these classes, __init__.py allows to use "import REGARDStoolbox.JSONmanager.*" to import several classes
more easily and allow to use them without the dot notation. This approach defines a list of modules we want to import
and within each module, import all the classes it can find.
"""

# list of modules to be imported
module_list = [
    "JSONelement",
    "JSONnull",
    "JSONfalse",
    "JSONtrue",
    "JSONnumber",
    "JSONstring",
    "JSONcollection",
    "JSONarray",
    "JSONobject",
    "JSONvalidation",
    "JSONexception",
]

# list of classe in the modules we want to make visible
class_list = [
    "JSONelement",
    "JSONnull",
    "JSONfalse",
    "JSONtrue",
    "JSONnumber",
    "JSONstring",
    "JSONcollection",
    "JSONarray",
    "JSONobject",
    "JSONvalidation",
    "JSONexception",
]

for module_name in module_list:

    # first of all, we import the module
    module = import_module(f"{__name__}.{module_name}")

    # then we scan the module to retrieve the list of items it contains
    for attribute_name in dir(module):
        attribute = getattr(module, attribute_name)

        # if the item is a class whose name belongs to class_list, we make it directly visible
        if isclass(attribute) and attribute_name in class_list:
            globals()[attribute_name] = attribute
