from .JSONelement import JSONelement
from .JSONvalidation import JSONvalidation


class JSONcollection(JSONelement):
    """
    JSON element corresponding to an collection (list of ordered or unordered elements). This class is a base
    class of SIPobject and SIParray and implements common methods
    """

    def __init__(self, data=None, validation=JSONvalidation()):
        """
        initialize an "in-memory" JSON collection with a provided data
        :param data: data (None or dictionary or JSONcollection)
        :param validation_level: type of validation
        """
        super().__init__(data, validation)

    def __len__(self):
        """
        return the number of items in the collection
        :return: number of items in the collection
        """
        return len(self._field__value)

    def __bool__(self):
        """
        check if the collection contains items (True) of not (False)
        :return: True if the collection contains items or False otherwise
        """
        return len(self) > 0
