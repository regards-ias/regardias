from .JSONelement import JSONelement
from .JSONexception import JSONexception
from .JSONvalidation import JSONvalidation


class JSONtrue(JSONelement):
    """
    JSON element corresponding to true
    """

    def __init__(self, data=True, validation=JSONvalidation()):
        """
        initialize an "in-memory" JSON true
        :param data: only True or JSONtrue expected (any other value raises an exception)
        :param validation: type of validation
        :exception JSONexception: data is neither True nor JSONtrue
        """
        super().__init__(data, validation)

    def validatedata(self, data, validation=JSONvalidation()):
        """
        validate the data used for assignment (True or JSONtrue expected)
        :param data: data to be validated
        :param validation: type of validation
        :exception JSONexception: data is neither True nor JSONtrue
        """
        if not (isinstance(data, JSONtrue) or data is True or data is None):
            raise JSONexception(f"cannot initialize JSONtrue with {data}")

    @classmethod
    def getstaticdefault(cls):
        """
        return a default "in-memory" JSONtrue
        :return: True
        """
        return True
