from .JSONelement import JSONelement
from .JSONexception import JSONexception
from .JSONvalidation import JSONvalidation


class JSONfalse(JSONelement):
    """
    JSON element corresponding to false
    """

    def __init__(self, data=False, validation=JSONvalidation()):
        """
        initialize an "in-memory" JSON false
        :param data: only False or JSONfalse expected (any other value raises an exception)
        :param validation: typr of validation
        :exception JSONexception: data is neither False nor JSONfalse
        """
        super().__init__(data, validation)

    def validatedata(self, data, validation=JSONvalidation()):
        """
        validate the data used for assignment (False or JSONfalse expected)
        :param data: data to be validated
        :param validation: type of validation
        :exception JSONexception: data is neither False nor JSONfalse
        """
        if not (isinstance(data, JSONfalse) or data is False or data is None):
            raise JSONexception(f"cannot initialize JSONfalse with {data}")

    @classmethod
    def getstaticdefault(cls):
        """
        return a default "in-memory" JSONfalse
        :return: False
        """
        return False
