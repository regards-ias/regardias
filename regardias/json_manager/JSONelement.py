import json
from copy import deepcopy

from .JSONexception import JSONexception
from .JSONvalidation import JSONvalidation


class JSONelement:
    """
    JSON element is the base class of JSON hierarchy. It prepares the mapping between JSON objects and Python
    objects based on the following equivalence table (Python->JSON):
    - dict (Python)	-> object (JSON)
    - list, tuple -> array
    - str -> string
    - int, long, float -> number
    - True ->true
    - False -> false
    - None -> null
    """

    indent = 2
    """
    ident used when dumping the object to a string stream (None allows to have a flat output and 0 allows
    to have elements left aligned, without indentation)
    """

    class CustomizedEncoder(json.JSONEncoder):
        """
        allows to dump data that contains JSONelement instances (may occur for example when array of JSONobject
        instances are assigned to a data field). Without that, dumping JSON elements may cause a serialization
        issue
        """

        def default(self, obj):
            """
            intercept the default processing of an item while dumping a JSON element to a text stream, to take
            into account elements derived from JSONelement
            :param obj: the object we want to dump to the text stream
            """
            if isinstance(obj, JSONelement):
                return obj._field__value
            return json.JSONEncoder.default(self, obj)

    def __init__(self, data=None, validation=JSONvalidation()):
        """
        initialize an "in-memory" JSONelement with a provided data. This method uses:
          - validatedata, to check that input data si compatible with the JSONelement (or derived)
          - updatefiledvalue, to update the field value
          - updatefielddescriptor, to add any necessary information that allows to deal with complex data
            (such as, for example, JSONobject)
        :param data: data (dict, list, str, int, float, True, False, None, JSONelement or derived)
        :param validation_level: level of validation (0: simplest validation)
        """
        self.validatedata(data, validation)
        self.updatefieldvalue(data)
        self.updatefielddescriptor(data)

    def validatedata(self, data, validation):
        """
        overriden in derived classes to check that data is compatible with the JSONelement (or derived)
        :param data: data to be validated
        :param validation: type of validation
        :exception JSONexception: validation has failed
        """
        pass

    def updatefieldvalue(self, data):
        """
        store data into the current instance
        :param data: data to be stored
        """
        if data is None:
            self._field__value = self.getdynamicdefault()
        elif isinstance(data, JSONelement):
            self._field__value = data._field__value
        else:
            self._field__value = data

    def updatefielddescriptor(self, data):
        """
        overriden in derived classes to update any necessary information (a.k.a filed descriptor) used to
        properly manage the data value (for example, fields types for a JSONobject)
        :param data: data used to build the filed descriptor
        """
        pass

    def __str__(self):
        """
        convert a JSONelement object into a textual JSON format, in a pretty printed way
        :return: the "in-memory" JSONelement in textual format
        """
        return json.dumps(
            self._field__value, indent=JSONelement.indent, cls=self.CustomizedEncoder
        )

    @classmethod
    def getstaticdefault(cls):
        return None

    def getdynamicdefault(self):
        return self.getstaticdefault()

    @classmethod
    def getJSONtype(cls, data):
        """
        get the JSONelement (or derived) type of a given data
        :param data: data we want to retrieve the JSONelement (or derived type=
        :return: data type (derived from JSONelement)
        :exception JSONexception: data is not compatible with JSONelement (or derived)
        """

        from .JSONnull import JSONnull
        from .JSONtrue import JSONtrue
        from .JSONfalse import JSONfalse
        from .JSONstring import JSONstring
        from .JSONnumber import JSONnumber
        from .JSONarray import JSONarray
        from .JSONobject import JSONobject

        if isinstance(data, JSONelement):
            return type(data)

        data_type = None

        if data is None:
            data_type = JSONnull
        elif isinstance(data, bool):
            data_type = JSONtrue if data else JSONfalse
        elif isinstance(data, str):
            data_type = JSONstring
        elif isinstance(data, int):
            data_type = JSONnumber
        elif isinstance(data, float):
            data_type = JSONnumber
        elif isinstance(data, dict):
            data_type = JSONobject
        elif isinstance(data, list):
            data_type = JSONarray
        else:
            raise JSONexception(f"cannot convert to JSONtype for {data}")

        return data_type

    @classmethod
    def toJSON(cls, data, validation=JSONvalidation()):
        """
        convert an item (data) into a JSONelement (or derived) (element)
        :param data: data to be converted into JSON element
        :param validation_level: type of validation
        :return: data converted into a JSON element
        :exception JSONexception: data cannot be converted to JSONelement (or derived)
        """

        if isinstance(data, JSONelement):
            return data

        data_type = cls.getJSONtype(data)

        return None if data_type is None else data_type(data, validation)

    def copy(self):
        """
        create a (deep) copy of the JSONelement (or derived)
        :return: (deep) copy of the JSONelement
        """
        return deepcopy(self)

    @classmethod
    def load(cls, json_stream, validation=JSONvalidation(recursive=True)):
        """
        retrieve a text in JSON format and store it as an "in-memory" JSONobject. The exact type of the returned
        instance depends on the strean content (for example, [1, 2, 3, 4] returns a JSONarray instance)
        :param json_stream: stream that "contains" the JSON object (as text)
        :param validation: type of validation
        :return: an "in memory" JSON object
        """

        data = json.load(json_stream)
        return cls.toJSON(data, validation)

    def dump(self, json_stream):
        """
        dump an "in-memory" JSON object into into a text stream
        :param json_stream: a text stream
        """
        json.dump(
            self._field__value,
            json_stream,
            indent=JSONelement.indent,
            cls=self.CustomizedEncoder,
        )

    def reset(self):
        """
        reset the JSONelement to its default value (see getdynamicdefaults for more details)
        """

        data = self.getdynamicdefault()

        self.validatedata(data)
        self.updatefieldvalue(data)
        self.updatefielddescriptor(data)
