from .JSONelement import JSONelement
from .JSONexception import JSONexception
from .JSONvalidation import JSONvalidation


class JSONnull(JSONelement):
    """
    JSON element corresponding to null
    """

    def __init__(self, data=None, validation=JSONvalidation()):
        """
        initialize an "in-memory" JSON null
        :param data: only None or JSONnull expected (any other value raises an exception)
        :param validation: type of validation
        :exception JSONexception: data is neither None nor JSONnull
        """
        super().__init__(data, validation)

    def validatedata(self, data, validation=JSONvalidation()):
        """
        validate the data used for assignment (None or JSONnull expected)
        :param data: data to be validated
        :param validation: type of validation
        :exception JSONexception: data is neither None nor JSONnull
        """
        if not (isinstance(data, JSONnull) or data is None):
            raise JSONexception(f"cannot initialize JSONnull with {data}")

    @classmethod
    def getstaticdefault(cls):
        """
        return a default "in-memory" JSONnull
        :return: None
        """
        return None
