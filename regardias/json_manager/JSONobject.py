from .JSONcollection import JSONcollection
from .JSONexception import JSONexception
from .JSONvalidation import JSONvalidation


class JSONobject(JSONcollection):
    """
    JSON element corresponding to an object (that is: dict)
    """

    def __init__(self, data=None, validation=JSONvalidation(recursive=True)):
        """
        initialize an "in-memory" JSON object with a provided data
        :param data: data (None or dictionary or JSONobject)
        :param validation: type of validation
        """
        # self._field__descriptor = {}

        super().__init__(data, validation)

        # validation is performed on each object field
        if validation.recursive:
            self.validateobjectfields(validation)

    def validatedata(self, data, validation=JSONvalidation()):
        """
        validate the data used for assignment (dict, JSONobject or None expected)
        :param data: data to be validated
        :param validation: type of validation
        :exception JSONexception: data is not one of dict, JSONobject or None
        """
        if not (isinstance(data, JSONobject) or isinstance(data, dict) or data is None):
            raise JSONexception(f"cannot initialize JSONobject with {data}")

    def validateobjectfields(self, validation=JSONvalidation()):
        """
        recursively validate all the object fields
        :param validation: type of validation
        :exception JSONexception: one of the object fields is not JSON compatible
        """
        for i, j in self._field__value.items():
            self.validateobjectfield(i, j, validation)

    def validateobjectfield(self, key, value, validation=JSONvalidation()):
        """
        check that the object field is also JSON compatible (that is: there is nothing "nasty" deeply nested
        in the object fields
        :param key: key of the object field (may be used in derived classes)
        :param value: the value we want to check
        :param validation: type of validation
        :exception JSONexception: the array field is not JSON compatible
        """
        json_type = self.getJSONtype(value)

        if issubclass(json_type, JSONobject):
            json_type(value).validateobjectfields(validation)
        else:
            json_type(value, validation)

    def updatefielddescriptor(self, data):
        """
        update the filed descriptor base on data:
          - if data is a dict, toJSON is used to deduced type of each element of the dictionary
          - if data is a JSONobject, current field descriptor is updated with data field descriptor (both
            objects share the same field descriptor)
        :param data: data used to initialize the current instance
        """

        # for the time being, we recompute the data type at each time (toJSON)

        # if isinstance(data, dict):
        #     for i, j in data.items():
        #         self._field__descriptor[i] = None if j is None else self.getJSONtype(j)
        # elif isinstance(data, JSONobject):
        #     self._field__descriptor = data._field__descriptor[i]

    def __getattr__(self, name):
        """
        get the field value corresponding to (field) name ("dot" notation: d.type)
        :param name: field name
        :return: field value
        """

        if name in ("_field__value"):
            return super().__getattribute__(name)

        if name not in self._field__value:
            return super().__getattribute__(name)

        # return self._field__descriptor[name](self._field__value[name])
        return self.toJSON(self._field__value[name])

    def __setattr__(self, name, value):
        """
        update the field value for field "name" ("dot" notation: d.type = "FeatureCollection")
        :param name: field name
        :param value: field value
        """
        if name in ("_field__value", "_field__descriptor"):
            return super().__setattr__(name, value)

        self._field__value[name] = value
        # self._field__descriptor[name] = self.getJSONtype(value)

    def __delattr__(self, name):
        """
        delete the field "name" ("dot" notation: del d.type)
        :param name: field name
        """
        if name in ("_field__value", "_field__descriptor"):
            return

        del self._field__value[name]
        # del self._field__descriptor[name]

    def __getitem__(self, name):
        """
        get the field value corresponding to (field) name (index notation: d["type"])
        :param name: field name
        :return: field value
        """
        return self.__getattr__(name)

    def __setitem__(self, name, value):
        """
        update the field value for field "name" (index notation: d.=["type"] = "FeatureCollection")
        :param name: field name
        :param value: field value
        """
        self.__setattr__(name, value)

    def __delitem__(self, name):
        """
        delete the field "name" (index notation: del d["type"])
        :param name: field name
        """
        self.__delattr__(name)

    def __iter__(self):
        """
        allows to iterate on each object element (that is: for i in obj ...)
        :return: the current element made of a field name and a field value
        """
        for name, value in self._field__value.items():
            yield name, value

    @classmethod
    def getstaticdefault(cls):
        """
        return a default "in-memory" JSONobject
        :return: {}
        """
        return {}
