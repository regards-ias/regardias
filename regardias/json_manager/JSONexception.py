"""
This Python module contains all the exceptions (that will be) utilized by JSONmanager. For the time being
only one exception is defined (JSONexception) and will be used whenever necessary in other JSONmanager modules
"""


class JSONexception(Exception):
    """
    for the time being this is the only exception used by JSONmanager.
    """

    def __init__(self, value):
        """
        initialize a new exception
        :param value: an explanation message associated to this exception instance
        """
        self.value = value

    def __str__(self):
        """
        convert the exception into a readable string
        :return: readable string corresponding to the exception
        """
        return repr(self.value)
