import json
import os

import requests

from regardias.api import API


class AttributeAPIError(Exception):
    pass


class AttributeAPI(API):
    """A class to access Regards models API"""

    def __init__(
        self, instance_url: str, project_name: str, username: str, password: str
    ):
        super().__init__(instance_url, project_name, username, password)
        self.api_url = os.path.join(
            self.instance_url, "api/v1/rs-dam/models/attributes"
        )
        self.retrieve_token()
        self.headers = {
            "Authorization": f"Bearer {self.token}",
        }

    def delete_by_id(self, id: int):
        res = requests.delete(f"{self.api_url}/{id}", headers=self.headers)
        if not res.ok:
            raise AttributeAPIError(res.json()["messages"])
        else:
            print(f"Attribute {id} deleted successfully")

    def delete_by_name(self, name: str):
        attribute_id = self.get_id_from_name(name)
        if attribute_id is not None:
            self.delete_by_id(attribute_id)

    def delete_all(self):
        res = requests.get(self.api_url, headers=self.headers)
        for attribute in res.json():
            attribute_id = attribute["content"]["id"]
            try:
                self.delete_by_id(attribute_id)
            except AttributeAPIError:
                print(f"{attribute['content']['name']} could not be deleted")

    def get_id_from_name(self, name: str):
        res = requests.get(self.api_url, headers=self.headers)
        attribute_id = None
        for attribute in res.json():
            if attribute["content"]["name"] == name:
                attribute_id = attribute["content"]["id"]
        if attribute_id is None:
            print(f"{name} not found")
        return attribute_id
