import json
import os

import requests

from regardias.api import API


class DatasourceAPIError(Exception):
    pass


class DatasourceAPI(API):
    """A class to access Regards datasets API"""

    def __init__(
        self, instance_url: str, project_name: str, username: str, password: str
    ):
        super().__init__(instance_url, project_name, username, password)
        self.api_url = os.path.join(self.instance_url, "api/v1/rs-dam/datasources")
        self.retrieve_token()
        self.headers = {
            "Authorization": f"Bearer {self.token}",
        }

    def get_list(self):
        res = requests.get(self.api_url, headers=self.headers)
        if not res.ok:
            raise DatasourceAPIError(res.json()["messages"])
        else:
            return res.json()

    def delete_by_id(self, datasource_id):
        res = requests.delete(f"{self.api_url}/{datasource_id}", headers=self.headers)
        if not res.ok:
            raise DatasourceAPIError(res.json()["messages"])
        else:
            print(f"Datasource {datasource_id} deleted successfully")

    def delete_all(self):
        datasources = self.get_list()
        for datasource in datasources:
            datasource_id = datasource["content"]["businessId"]
            try:
                self.delete_by_id(datasource_id)
            except DatasourceAPIError as e:
                print(e)
