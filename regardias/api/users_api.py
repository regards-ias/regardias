import json
import os
from dataclasses import dataclass
from typing import List

import requests

from regardias.api import API


class UsersAPIError(Exception):
    pass


@dataclass
class UsersAPI(API):
    """A class to access Regards users API"""

    def __post_init__(self):
        self.api_url = os.path.join(self.instance_url, "api/v1/rs-admin/users")
        self.retrieve_token()
        self.headers = {
            "Authorization": f"Bearer {self.token}",
        }

    def add_access_group_for_user_by_email(self, email: str, new_access_group: str):
        user_id = self.get_user_id_from_email(email)
        self.add_access_group_for_user_by_id(user_id, new_access_group)

    def add_access_group_for_user_by_id(self, user_id: int, new_access_group: str):
        r = requests.get(f"{self.api_url}/{user_id}", headers=self.headers)
        if not r.ok:
            raise UsersAPIError(r.json()["messages"])
        user = r.json()["content"]
        access_groups = user["accessGroups"]
        if new_access_group not in access_groups:
            access_groups.append(new_access_group)
            self.set_access_groups_for_user(user, access_groups)
        else:
            print(
                f"User {user['firstName']} {user['lastName']} ({user['id']}) already has access to {new_access_group}"
            )

    def get_access_groups_for_user_by_id(self, user_id: int):
        r = requests.get(f"{self.api_url}/{user_id}", headers=self.headers)
        if not r.ok:
            raise UsersAPIError(r.json()["messages"])
        user = r.json()["content"]
        return user["accessGroups"]

    def get_access_groups_for_user_by_email(self, email: str):
        user_id = self.get_user_id_from_email(email)
        return self.get_access_groups_for_user_by_id(user_id)

    def get_user_id_from_email(self, email: str):
        r = requests.get(f"{self.api_url}/email/{email}", headers=self.headers)
        if not r.ok:
            raise UsersAPIError(r.json()["messages"])
        user = r.json()["content"]
        return user["id"]

    def remove_access_group_for_user_by_email(self, email: str, access_group: str):
        user_id = self.get_user_id_from_email(email)
        self.remove_access_group_for_user_by_id(user_id, access_group)

    def remove_access_group_for_user_by_id(self, user_id: int, access_group: str):
        r = requests.get(f"{self.api_url}/{user_id}", headers=self.headers)
        if not r.ok:
            raise UsersAPIError(r.json()["messages"])
        user = r.json()["content"]
        access_groups = user["accessGroups"]
        if access_group in access_groups:
            access_groups.remove(access_group)
            self.set_access_groups_for_user(user, access_groups)
        else:
            print(
                f"User {user['firstName']} {user['lastName']} ({user['id']}) does not have access to {access_group}"
            )

    def set_access_groups_for_user(self, user: dict, access_groups: List[str]):
        user_id = user["id"]
        user["accessGroups"] = access_groups
        headers = {
            "Content-type": "application/json;charset=UTF-8",
            "Accept": "application/json",
            **self.headers,
        }
        r = requests.put(
            f"{self.api_url}/{user_id}", headers=headers, data=json.dumps(user)
        )
        if not r.ok:
            raise UsersAPIError(r.json()["messages"])
        else:
            print(
                f"User {user['firstName']} {user['lastName']} ({user['id']}) was granted access to groups {access_groups}"
            )

    def set_access_groups_for_user_by_email(self, email: str, access_groups: List[str]):
        user_id = self.get_user_id_from_email(email)
        self.set_access_groups_for_user_by_id(user_id, access_groups)

    def set_access_groups_for_user_by_id(self, user_id: int, access_groups: List[str]):
        if "Public" not in access_groups:
            access_groups.append("Public")
        r = requests.get(f"{self.api_url}/{user_id}", headers=self.headers)
        if not r.ok:
            raise UsersAPIError(r.json()["messages"])
        user = r.json()["content"]
        self.set_access_groups_for_user(user, access_groups)
