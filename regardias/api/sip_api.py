import json
from pathlib import Path

import requests

from regardias.api import API
from regardias.tools.custom_types import PathLike
from regardias.tools.custom_types import PathLikeIterator


class SIPAPIError(Exception):
    pass


class SIPAPI(API):
    """A class to access Regards models API"""

    def __init__(
        self, instance_url: str, project_name: str, username: str, password: str
    ):
        super().__init__(instance_url, project_name, username, password)
        self.api_url = f"{self.instance_url}/api/v1/rs-ingest/sips"
        self.retrieve_token()
        self.headers = {
            "Authorization": f"Bearer {self.token}",
        }

    def ingest_sip(self, sip_filename: PathLike):
        sip_filename = Path(sip_filename)
        if not sip_filename.exists():
            raise FileExistsError("The SIP file you want to ingest does not exists")
        headers = {"Content-Type": "application/geo+json;charset=UTF-8", **self.headers}
        with open(sip_filename.resolve().as_posix()) as sip_file:
            sip_dict = json.load(sip_file)
        res = requests.post(self.api_url, headers=headers, json=sip_dict)
        if not res.ok:
            raise SIPAPIError(res.json()["messages"])
        else:
            print(f"Ingestion of {sip_filename.name} successful")

    def ingest_multiple_sips(self, sip_filenames: PathLikeIterator):
        for sip_filename in sip_filenames:
            try:
                self.ingest_sip(sip_filename)
            except (FileExistsError, SIPAPIError) as e:
                print(f"Ingestion failed for {sip_filename}: {e}")

    def ingest_multiple_sips_from_directory(self, directory: PathLike):
        directory = Path(directory)
        sip_filenames = directory.glob("*.json")
        self.ingest_multiple_sips(sip_filenames)

    def ingest_multiple_sips_from_filelist(self, filelist: PathLike):
        filelist = Path(filelist)
        sip_filenames = filelist.read_text().splitlines()
        self.ingest_multiple_sips(sip_filenames)

    def ingest_multiple_sips_from_file(self, file: PathLike):
        with open(file, "r") as f:
            sip_filenames = f.readlines()
        self.ingest_multiple_sips(sip_filenames)
