import json
from dataclasses import dataclass
from pathlib import Path
from typing import Optional
from typing import Union

import requests

from regardias.tools.custom_types import PathLike


class APIError(Exception):
    pass


@dataclass
class API:
    """A class to access Regards API"""

    instance_url: str
    project_name: str
    username: str
    password: str
    token: Optional[str] = None

    def retrieve_token(self):
        """Retrieve a token to access the API with username/password"""
        api_url = f"{self.instance_url}/api/v1/rs-authentication/oauth/token?grant_type=password&username={self.username}&password={self.password}&scope={self.project_name}"
        headers = {"authorization": "Basic Y2xpZW50OnNlY3JldA=="}
        res = requests.post(api_url, headers=headers)
        if not res.ok:
            raise APIError(f"{res.json()['detailMessage']}")
        self.token = res.json()["access_token"]

    def export_microservice_configuration(
        self, microservice: str, filename: Optional[PathLike] = None
    ):
        """Export a microservice configuration to a JSON file"""
        if filename is None:
            filename = f"config-{microservice}.json"
        api_url = (
            f"{self.instance_url}/api/v1/{microservice}/microservice/configuration"
        )
        self.retrieve_token()
        headers = {
            "Authorization": f"Bearer {self.token}",
        }
        res = requests.get(api_url, headers=headers)
        if not res.ok:
            raise APIError(res.text)
        else:
            with open(filename, "w") as file:
                json.dump(res.json(), file, indent=4)
            print(f"Configuration exported successfully to {filename}")

    def import_microservice_configuration(self, microservice: str, filename: str):
        """Import a microservice configuration from a JSON file"""
        api_url = (
            f"{self.instance_url}/api/v1/{microservice}/microservice/configuration"
        )
        self.retrieve_token()
        headers = {
            "Authorization": f"Bearer {self.token}",
        }
        with open(filename, "rb") as f:
            files = {
                "file": (
                    filename.split("/")[-1],
                    f,
                    "application/json",
                    {"Expires": "0"},
                )
            }
            res = requests.post(api_url, headers=headers, files=files)
            if not res.ok:
                raise APIError(res.text)
            else:
                print(f"Configuration imported successfully from {filename}")
