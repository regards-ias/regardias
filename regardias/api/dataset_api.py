import json
import os

import requests

from regardias.api import API


class DatasetAPIError(Exception):
    pass


class DatasetAPI(API):
    """A class to access Regards datasets API"""

    def __init__(
        self, instance_url: str, project_name: str, username: str, password: str
    ):
        super().__init__(instance_url, project_name, username, password)
        self.api_url = os.path.join(self.instance_url, "api/v1/rs-dam/datasets")
        self.retrieve_token()
        self.headers = {
            "Authorization": f"Bearer {self.token}",
        }

    def get_id_from_name(self, dataset_name):
        datasets = self.get_list()
        for dataset in datasets:
            if dataset["content"]["feature"]["label"] == dataset_name:
                return dataset["content"]["id"]
        raise DatasetAPIError(f"Dataset {dataset_name} not found")

    def get_list(self):
        res = requests.get(self.api_url, headers=self.headers)
        if not res.ok:
            raise DatasetAPIError(res.json()["messages"])
        else:
            datasets = res.json()["content"]
            return datasets

    def list_available_datasets(self):
        datasets = self.get_list()
        return [dataset["content"]["feature"]["label"] for dataset in datasets]

    def delete_by_id(self, dataset_id):
        res = requests.delete(f"{self.api_url}/{dataset_id}", headers=self.headers)
        if not res.ok:
            raise DatasetAPIError(res.json()["messages"])

    def delete_by_name(self, dataset_name):
        try:
            dataset_id = self.get_id_from_name(dataset_name)
            self.delete_by_id(dataset_id)
            print(f"Dataset {dataset_name} deleted successfully")
        except DatasetAPIError as e:
            print(e)

    def delete_all(self):
        datasets = self.get_list()
        for dataset in datasets:
            dataset_id = dataset["content"]["id"]
            try:
                self.delete_by_id(dataset_id)
            except DatasetAPIError as e:
                print(e)
