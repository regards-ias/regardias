import os
from pathlib import Path
from typing import List
from typing import Optional

import requests

from regardias.api import API
from regardias.data_management import Model
from regardias.tools.custom_types import PathLike


class ModelAPIError(Exception):
    pass


class ModelAPI(API):
    """A class to access Regards models API"""

    def __init__(
        self, instance_url: str, project_name: str, username: str, password: str
    ):
        super().__init__(instance_url, project_name, username, password)
        self.api_url = os.path.join(self.instance_url, "api/v1/rs-dam/models")
        self.retrieve_token()
        self.headers = {
            "Authorization": f"Bearer {self.token}",
        }

    def delete_all(self):
        models = self.get_list()
        for model in models:
            self.delete_by_name(model.name)

    def delete_by_name(self, model_name: str):
        url = os.path.join(self.api_url, model_name)
        res = requests.delete(url, headers=self.headers)
        if not res.ok:
            raise ModelAPIError(res.json()["messages"])
        else:
            print(f"{model_name} deleted successfully")

    def export_to_xml(self, model_name: str, filename: Optional[PathLike] = None):
        if filename is None:
            f"{model_name}.xml"
        filename = Path(filename)
        url = os.path.join(self.api_url, model_name, "export")
        res = requests.get(url, headers=self.headers)
        if not res.ok:
            raise ModelAPIError(res.json()["messages"])
        else:
            filename.write_text(res.text)
            print(f"{model_name} exported successfully")

    def get_list(self) -> List[Model]:
        res = requests.get(self.api_url, headers=self.headers)
        if not res.ok:
            raise ModelAPIError(res.json()["messages"])
        models = []
        for model_dict in res.json():
            models.append(Model(**model_dict["content"]))
        return models

    def import_from_xml(self, model_filename: PathLike):
        model_filename = Path(model_filename)
        with open(model_filename, "r") as model_file:
            files = {"file": (model_filename.name, model_file, "text/xml")}
            res = requests.post(
                f"{self.api_url}/import", headers=self.headers, files=files
            )
        if not res.ok:
            raise ModelAPIError(res.json()["messages"])
        else:
            print(f"{model_filename} ingested successfully")
