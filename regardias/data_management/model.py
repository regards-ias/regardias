from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path
from typing import List
from typing import Optional

from lxml import etree as ET

from regardias.data_management.attribute import Attribute
from regardias.tools.custom_types import PathLike
# from lxml.etree import ElementTree as ET


@dataclass
class Model:
    """A Model in Regards sense

    See https://regardsoss.github.io/docs/user-guide/data-organization/models/ for more details.
    """

    id: int
    name: str
    description: str
    type: str
    attributes: Optional[List[Attribute]] = None

    def __repr__(self) -> str:
        representation = ""
        representation += f"Regards {self.type} model {self.name}:\n"
        representation += f"{self.description}\n\n"
        representation += f"Contains {len(self.attributes)} attributes:\n"
        for attribute in self.attributes:
            representation += f"\t- {attribute.name} ({attribute.attribute_type}): {attribute.description})\n"
        return representation

    @classmethod
    def from_xml(cls, filename: PathLike) -> Model:
        """Create a Model from an XML file"""
        filename = Path(filename)
        xml_tree = ET.parse(filename.resolve().as_posix())
        xml_root = xml_tree.getroot()

        # Get model metadata
        name = xml_root.findtext("name")
        description = xml_root.findtext("description")
        type = xml_root.findtext("type")

        # Get model attributes
        attributes = list(
            map(Attribute.from_xml_element, xml_root.findall("attribute"))
        )

        return cls(None, name, description, type, attributes)

    def to_xml(self, filename: PathLike):
        """Write the Model to an XML file"""
        filename = Path(filename)
        xml_root = ET.Element("model")
        ET.SubElement(xml_root, "name").text = self.name
        ET.SubElement(xml_root, "description").text = self.description
        ET.SubElement(xml_root, "type").text = self.type
        for attribute in self.attributes:
            attribute_element = attribute.to_xml_element()
            xml_root.append(attribute_element)
        xml_tree = ET.ElementTree(xml_root)
        with open(filename.resolve().as_posix(), "wb") as file:
            file.write(b'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n')
            file.write(ET.tostring(xml_tree, encoding="utf-8", pretty_print=True))
            file.write(b"\n")
