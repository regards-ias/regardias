from dataclasses import dataclass

from lxml import etree as ET


@dataclass
class Attribute:
    """An Attribute in Regards sense"""

    label: str
    name: str
    description: str
    attribute_type: str
    attribute_unit: str
    alterable: bool = True
    optional: bool = True
    indexed: bool = True

    @classmethod
    def from_xml_element(cls, element):
        label = element.findtext("label")
        name = element.findtext("name")
        description = element.findtext("description")
        attribute_type = element.findtext("type")
        attribute_unit = element.find("type").get("unit")
        alterable = element.get("alterable") == "true"
        optional = element.get("optional") == "true"
        indexed = element.get("indexed") == "true"
        return cls(
            label,
            name,
            description,
            attribute_type,
            attribute_unit,
            alterable,
            optional,
            indexed,
        )

    def to_xml_element(self):
        element = ET.Element("attribute")
        ET.SubElement(element, "label").text = self.label
        ET.SubElement(element, "name").text = self.name
        ET.SubElement(element, "description").text = self.description
        type_element = ET.SubElement(element, "type")
        type_element.text = self.attribute_type
        type_element.set("unit", self.attribute_unit)
        element.set("alterable", str(self.alterable).lower())
        element.set("optional", str(self.optional).lower())
        element.set("indexed", str(self.indexed).lower())
        return element
