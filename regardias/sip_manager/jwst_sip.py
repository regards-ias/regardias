from pathlib import Path
from typing import Union

from astropy.io import fits
from PIL import Image

from regardias.sip_manager import SIP
from regardias.tools import compute_md5sum


FILEPATH_ROOT = "file://"
EMPTY_THUMBNAIL = "/data/ceph-data/JWST/REGARDS_DATA/empty.png"


class JWSTSIPError(Exception):
    pass


class JWSTSIP(SIP):
    """A class to handle JWST specific SIP objects"""

    def update_from_fits_file(self, fits_filename: Union[str, Path]):
        """Use the metadata from a FITS file to update the SIP object"""
        fits_filename = Path(fits_filename)
        fits_header = fits.getheader(fits_filename.resolve().as_posix())
        features = self._json_object.features[0]

        # Get ID and version of the product
        product_version = fits_filename.name.replace(".fits", "").split("_")[-1]
        product_id = fits_filename.name.replace(f"_{product_version}.fits", "")

        # Update the main object information (id and coordinates)
        features.id = product_id
        RA = fits_header.get("TARG_RA", -1)
        DEC = fits_header.get("TARG_DEC", -1)
        features.geometry.coordinates = [RA, DEC]

        # Update the metadata relative to the file itself
        file_data_object = features.properties.contentInformations[0].dataObject
        file_data_object.filename = fits_filename.name
        file_data_object.locations[
            0
        ].url = f"{FILEPATH_ROOT}{fits_filename.resolve().as_posix()}"
        file_data_object.checksum = compute_md5sum(fits_filename)
        file_data_object.fileSize = fits_filename.stat().st_size

        # Update the metadata relative to the thumbnail if it does exists
        thumbnail_filename = fits_filename.with_suffix(".png")
        if not thumbnail_filename.exists():
            thumbnail_filename = Path(EMPTY_THUMBNAIL)
        for i in [1, 2]:
            thumbnail_data_object = features.properties.contentInformations[
                i
            ].dataObject
            thumbnail_data_object.filename = thumbnail_filename.name
            thumbnail_data_object.locations[
                0
            ].url = f"{FILEPATH_ROOT}{thumbnail_filename.resolve().as_posix()}"
            thumbnail_data_object.checksum = compute_md5sum(thumbnail_filename)
            thumbnail_data_object.fileSize = thumbnail_filename.stat().st_size
            thumbnail_representation_information = (
                features.properties.contentInformations[i].representationInformation
            )
            image = Image.open(thumbnail_filename)
            width, height = image.size
            thumbnail_representation_information.syntax.width = width
            thumbnail_representation_information.syntax.height = height

        # Update the metadata relative to the README if it exists
        readme_filename = fits_filename.with_stem(
            f"README_{fits_filename.stem}"
        ).with_suffix(".txt")
        if readme_filename.exists():
            readme_data_object = features.properties.contentInformations[3].dataObject
            readme_data_object.filename = readme_filename.name
            readme_data_object.locations[
                0
            ].url = f"{FILEPATH_ROOT}{readme_filename.resolve().as_posix()}"
            readme_data_object.checksum = compute_md5sum(readme_filename)
            readme_data_object.fileSize = readme_filename.stat().st_size

        # Update the metadata of the data, contained in the FITS header
        metadata = features.properties.descriptiveInformation
        for keyword in metadata._field__value.keys():
            if keyword == "DATE-OBS":
                date = fits_header.get("DATE-OBS")
                time = fits_header.get("TIME-OBS")
                value = f"{date}T{time}"
            elif keyword == "BKGDTARG":
                if fits_header[keyword]:
                    value = "Background"
                else:
                    value = "Science"
            elif keyword == "PI_NAME":
                value = fits_header.get(keyword).split(",")[0]
            elif keyword == "VERSION":
                value = product_version
            else:
                value = fits_header.get(keyword, "")
            metadata._field__value[keyword] = value

    def to_json_file(self, filename: Union[str, Path]):
        filename = Path(filename)
        with open(filename.resolve().as_posix(), "w") as sip_file:
            self._json_object.dump(sip_file)
