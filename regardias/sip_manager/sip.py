from pathlib import Path
from typing import Union

from astropy.io import fits
from PIL import Image

from regardias.json_manager import JSONobject
from regardias.tools import compute_md5sum
from regardias.tools.custom_types import PathLike


class SIPError(Exception):
    pass


class SIP:
    """A class to handle SIP objects"""

    def __init__(self, json_object: JSONobject):
        self._json_object = json_object

    @classmethod
    def from_template(cls, template_filename: PathLike):
        """Create a SIP object from a template file.

        It can be used to instantiate a dummy SIP object that will be populated later on, using metadata from
        an external file."""
        template_filename = Path(template_filename)
        with open(template_filename.resolve().as_posix(), "r") as template_file:
            return cls(JSONobject.load(template_file))

    def update_from_fits_file(self, fits_filename: Union[str, Path]):
        """Use the metadata from a FITS file to update the SIP object"""
        fits_filename = Path(fits_filename)
        fits_header = fits.getheader(fits_filename.resolve().as_posix())
        features = self._json_object.features[0]

        # Update the main object information (id and coordinates)
        features.id = fits_filename.name.strip(".fits")
        features.geometry.coordinates = [
            fits_header.get("TARG_RA", -1),
            fits_header.get("TARG_DEC", -1),
        ]

        # Update the metadata relative to the file itself
        file_metadata = features.properties.contentInformations[0].dataObject
        file_metadata.filename = fits_filename.name
        file_metadata.locations[0].url = "file://{}".format(
            fits_filename.absolute().resolve().as_posix()
        )
        file_metadata.checksum = compute_md5sum(fits_filename)
        file_metadata.fileSize = fits_filename.stat().st_size

        # Update the metadata relative to the thumbnail if it does exists
        thumbnail_filename = fits_filename.with_suffix(".png")
        if not thumbnail_filename.exists():
            raise SIPError("The thumbnail is not available, SIP can not be created")
        else:
            for i in [1, 2]:
                thumbnail_data_object = features.properties.contentInformations[
                    i
                ].dataObject
                thumbnail_data_object.filename = thumbnail_filename.name
                thumbnail_data_object.locations[0].url = "file://{}".format(
                    thumbnail_filename.resolve().as_posix()
                )
                thumbnail_data_object.checksum = compute_md5sum(thumbnail_filename)
                thumbnail_data_object.fileSize = thumbnail_filename.stat().st_size
                thumbnail_representation_information = (
                    features.properties.contentInformations[i].representationInformation
                )
                image = Image.open(thumbnail_filename)
                width, height = image.size
                thumbnail_representation_information.syntax.width = width
                thumbnail_representation_information.syntax.height = height

        # Update the metadata of the data, contained in the FITS header
        metadata = features.properties.descriptiveInformation
        for keyword in metadata._field__value.keys():
            metadata._field__value[keyword] = fits_header.get(keyword, "")

    def to_json_file(self, filename: PathLike):
        filename = Path(filename)
        with open(filename.resolve().as_posix(), "w") as sip_file:
            self._json_object.dump(sip_file)
