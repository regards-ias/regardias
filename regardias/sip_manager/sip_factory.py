from regardias.sip_manager import JWSTSIP
from regardias.sip_manager import SIP
from regardias.tools.custom_types import PathLike


class SIPFactoryError(Exception):
    pass


class SIPFactory:
    """A factory class to create SIP objects"""

    @staticmethod
    def from_template(template_filename: PathLike, project_name: str = None):
        """Create a SIP object from a template file.

        It can be used to instantiate a dummy SIP object that will be populated later on, using metadata from
        an external file.

        If project_name is None (default) use the default SIP class that will not apply any processing to populate the SIP. For other projects (for now, only JWST is available), the derived classs allows to have some sepecific pre-processing when populating the SIP template.
        """
        if project_name is None:
            return SIP.from_template(template_filename)
        elif project_name == "JWST":
            return JWSTSIP.from_template(template_filename)
        else:
            raise SIPFactoryError("The provided project name is not valid.")
