import pytest

from regardias.api import APIError


class TestAPI:
    def test_retrieve_token(self, api):
        api.retrieve_token()

    def test_retrieve_token_failed(self, api):
        api.password = "wrong_password"
        with pytest.raises(APIError) as e_info:
            api.retrieve_token()
