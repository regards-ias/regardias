import os

import pytest
from dotenv import load_dotenv

from regardias.api import API
from regardias.api import UsersAPI


@pytest.fixture
def api():
    environment_is_set = load_dotenv(".env_tests")
    if not environment_is_set and os.environ.get("REGARDS_INSTANCE_URL") is None:
        raise ValueError("Environment variables not set")
    return API(
        os.environ["REGARDS_INSTANCE_URL"],
        os.environ["REGARDS_PROJECT"],
        os.environ["REGARDS_USERNAME"],
        os.environ["REGARDS_PASSWORD"],
    )


@pytest.fixture
def user_api():
    environment_is_set = load_dotenv(".env_tests")
    if not environment_is_set and os.environ.get("REGARDS_INSTANCE_URL") is None:
        raise ValueError("Environment variables not set")
    return UsersAPI(
        os.environ["REGARDS_INSTANCE_URL"],
        os.environ["REGARDS_PROJECT"],
        os.environ["REGARDS_USERNAME"],
        os.environ["REGARDS_PASSWORD"],
    )


@pytest.fixture
def correct_username():
    return "regards-test@universite-paris-saclay.fr"


@pytest.fixture
def incorrect_username():
    return "toto@universite-paris-saclay.fr"


@pytest.fixture
def correct_access_group():
    return "Test"


@pytest.fixture
def default_access_group():
    return "Public"


@pytest.fixture
def incorrect_access_group():
    return "Toto"
