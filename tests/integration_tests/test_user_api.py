import os

import dotenv
import pytest

from regardias.api import UsersAPIError


class TestUserAPI:
    def test_init(self, user_api):
        assert user_api.api_url.endswith("api/v1/rs-admin/users")

    def test_add_access_group_by_email(
        self, user_api, correct_username, correct_access_group
    ):
        user_api.add_access_group_for_user_by_email(
            correct_username, correct_access_group
        )
        assert "Test" in user_api.get_access_groups_for_user_by_email(correct_username)

    def test_add_access_group_by_email_wrong_email(
        self, user_api, incorrect_username, default_access_group
    ):
        with pytest.raises(UsersAPIError) as e_info:
            user_api.add_access_group_for_user_by_email(
                incorrect_username, default_access_group
            )
        assert (
            "Entity fr.cnes.regards.modules.accessrights.domain.projects.ProjectUser with id : toto@universite-paris-saclay.fr doesn't exist"
            in str(e_info.value)
        )

    def test_add_access_group_by_email_non_existent_group(
        self, user_api, correct_username, incorrect_access_group
    ):
        dotenv.load_dotenv()
        with pytest.raises(UsersAPIError) as e_info:
            user_api.add_access_group_for_user_by_email(
                correct_username, incorrect_access_group
            )
        assert (
            "Entity fr.cnes.regards.modules.dam.domain.dataaccess.accessgroup.AccessGroup with id : Toto doesn't exist"
            in str(e_info.value)
        )

    def test_remove_access_group_by_email(
        self, user_api, correct_username, correct_access_group
    ):
        user_api.remove_access_group_for_user_by_email(
            correct_username, correct_access_group
        )
        assert "Test" not in user_api.get_access_groups_for_user_by_email(
            correct_username
        )

    def test_remove_access_group_by_email_wrong_email(
        self, user_api, incorrect_username, default_access_group
    ):
        with pytest.raises(UsersAPIError) as e_info:
            user_api.remove_access_group_for_user_by_email(
                incorrect_username, default_access_group
            )
        assert (
            "Entity fr.cnes.regards.modules.accessrights.domain.projects.ProjectUser with id : toto@universite-paris-saclay.fr doesn't exist"
            in str(e_info.value)
        )

    def test_set_access_groups_by_email(
        self, user_api, correct_username, default_access_group, correct_access_group
    ):
        old_access_groups = user_api.get_access_groups_for_user_by_email(
            correct_username
        )
        test_access_groups = [default_access_group, correct_access_group]
        user_api.set_access_groups_for_user_by_email(
            correct_username, test_access_groups
        )
        assert set(test_access_groups) == set(
            user_api.get_access_groups_for_user_by_email(correct_username)
        )
        user_api.set_access_groups_for_user_by_email(
            correct_username, old_access_groups
        )
