import os
from regardias.data_management.model import Model
from regardias.tools.custom_types import PathLike
from xmldiff import main



class TestModel:
    def test_read_model(self):
        model = Model.from_xml("./tests/tests_data/data_model.xml")
        assert model.type == "DATA"
        assert model.name == "JWST_science_data"
        assert model.description == "JWST science products data model"
        assert len(model.attributes) == 25

    def test_read_write_model(self):
        model = Model.from_xml("./tests/tests_data/data_model.xml")
        model.to_xml("./tests/tests_data/output_model.xml")
        diff = main.diff_files("./tests/tests_data/data_model.xml", "./tests/tests_data/output_model.xml")
        assert len(diff) == 0
        os.remove("./tests/tests_data/output_model.xml")