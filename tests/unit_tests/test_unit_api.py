import json
from unittest.mock import MagicMock

import pytest
import requests_mock

from regardias.api import API
from regardias.api import APIError


class TestAPI:
    """A class to test the API class"""

    def test_init(self):
        api = API(
            "https://somerandom_url.ias.u-psud.fr/",
            "my_project",
            "user@ias.u-psud.fr",
            "password",
        )
        assert api.instance_url == "https://somerandom_url.ias.u-psud.fr/"
        assert api.project_name == "my_project"
        assert api.username == "user@ias.u-psud.fr"
        assert api.password == "password"
        assert api.token is None

    def test_init_failed(self):
        """Wrong number of arguments"""
        with pytest.raises(TypeError) as e_info:
            api = API(
                "https://somerandom_url.ias.u-psud.fr/",
                "my_project",
                "user@ias.u-psud.fr",
            )

    def test_retrieve_token(self, api):
        with requests_mock.Mocker() as m:
            m.post(
                f"{api.instance_url}/api/v1/rs-authentication/oauth/token?grant_type=password&username={api.username}&password={api.password}&scope={api.project_name}",
                status_code=200,
                json={"access_token": "my_token"},
            )
            api.retrieve_token()
        assert api.token == "my_token"

    def test_retrieve_token_failed(self, api):
        with requests_mock.Mocker() as m:
            m.post(
                f"{api.instance_url}/api/v1/rs-authentication/oauth/token?grant_type=password&username={api.username}&password={api.password}&scope={api.project_name}",
                status_code=400,
                json={"detailMessage": "Some error message"},
            )
            with pytest.raises(APIError) as e_info:
                api.retrieve_token()

    def test_export_microservice_configuration_failed(self, api, microservice_name):
        api.retrieve_token = MagicMock()
        api.token = "my_token"
        api_url = (
            f"{api.instance_url}/api/v1/{microservice_name}/microservice/configuration"
        )
        headers = {
            "Authorization": f"Bearer {api.token}",
        }
        with requests_mock.Mocker() as m:
            m.get(
                api_url,
                headers=headers,
                status_code=400,
                json={"detailMessage": "Some error message"},
            )
            with pytest.raises(APIError) as e_info:
                api.export_microservice_configuration(microservice_name)

    def test_export_microservice_configuration(self, api, microservice_name, tmp_path):
        filename = tmp_path / f"config-{microservice_name}.json"
        json_input = {"foo": "bar", "bar": "foo"}
        api.retrieve_token = MagicMock()
        api.token = "my_token"
        api_url = (
            f"{api.instance_url}/api/v1/{microservice_name}/microservice/configuration"
        )
        headers = {
            "Authorization": f"Bearer {api.token}",
        }
        with requests_mock.Mocker() as m:
            m.get(api_url, headers=headers, status_code=200, json=json_input)
            api.export_microservice_configuration(microservice_name, filename)
        assert filename.exists()
        json_output = json.loads(filename.read_text())
        assert json_output == json_input

    # def test_import_microservice_configuration_failed(self, api, microservice_name, microservice_configuration_file):
    #     api.retrieve_token = MagicMock()
    #     api.token = "my_token"
    #     api_url = f"{api.instance_url}/api/v1/{microservice_name}/microservice/configuration"
    #     headers = {
    #         "Authorization": f"Bearer {api.token}",
    #     }
    #     with open(microservice_configuration_file, "rb") as f:
    #         files = {"file": (microservice_configuration_file.name, f, "application/json", {'Expires': '0'})}
    #         with requests_mock.Mocker() as m:
    #             m.post(api_url, headers=headers, files=files, status_code=400, text="Some error message")
    #             with pytest.raises(APIError) as e_info:
    #                 api.import_microservice_configuration(microservice_name)
