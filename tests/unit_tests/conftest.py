import json
from unittest.mock import Mock

import pytest

from regardias.api import API


@pytest.fixture
def api():
    return API(
        "https://somerandom_url.ias.u-psud.fr/",
        "my_project",
        "user@ias.u-psud.fr",
        "password",
    )


@pytest.fixture
def microservice_configuration_file(tmp_path):
    filename = tmp_path / f"config-microservice-name.json"
    json_input = {"foo": "bar", "bar": "foo"}
    with filename.open("w") as f:
        json.dump(json_input, f)
    return filename


@pytest.fixture
def microservice_name():
    return "microservice_name"
