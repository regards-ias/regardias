# Authors

For now, this project is developed by Loic Maurin ([@lmaurin](https://git.ias.u-psud.fr/lmaurin)), except for the `json_manager` part that has been developed by Jean-Charles Pouplard at CNES. For now, Jean-Charles sent us his module by email as a collection of non-installable Python files. For this reason, they have been included as part of **RegardIAS**. As soon as it can be used as a standalone library, it will be removed from **RegardIAS** and included as a dependency to avoid any confusion.
