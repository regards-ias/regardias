# Dataset tools

The `dataset` group contains only the `delete` command for now. You can delete one, several or all datasets for a given project. You need to know the dataset id to delete it. Use `regardias dataset --help` for more information.


*Examples:*
* `regardias -e .env_file dataset delete -d dataset_id`
* `regardias -e .env_file dataset delete -d some_dataset_id -d another_dataset_id`
* `regardias -e .env_file dataset delete --all`
