# Project tools

The `project` group contains commands to administrate a Regards project. Use `regardias project --help` for more information.
Available commands allow to export/import individual microservices configuration or full project configuration (same as exporting/import each microservice configuration in the GUI). Only users and users access rights are not exported/imported. You can also backup the configuration to a Gitlab repository. Exportation of the configuration stores all files in the `.tar.gz` on your disk while backup allows you to commit and push to a Gitlab repository. It is used for automatic backup through Gitlab-CI. It is also possible to purge a project, deleting models, attributes, datasets, crawlers, IHM configuration, etc.

*Examples:*

* `regardias -e .jwst_qualif project purge`
* `regardias -e .jwst_qualif project export-full-config -o regards-pp.tar.gz`
* `regardias -e .jwst_qualif project import-full-config -f regards-pp.tar.gz`
* `regardias -e .jwst_qualif project backup-full-config -r https://git.ias.u-psud.fr/regards-ias/regards-pp-jwst-config`
