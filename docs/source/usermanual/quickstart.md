# Quickstart

The library is thought as both a collection of tools that can be used from other libraries and a command-line tool for Regards related activities. They are currently developed for JWST project purposes, so some of the scripts are not as generic as they should be. It will evolve according to the necessities of other projects.
The CLI has several collections of tools to deal with:
* attributes
* data
* datasets
* models
* projects
* sip
* users

You can use `regardias --help` for more detailed information about the available commands, and `regardias command --help` for specific command information.

To interact with a Regards instance, you can either provide an environment file containing its url, project name and admin credentials using the `regardias -e .env_file my_command ...` or pass them as individual arguments `regardias -i instance_url -n project_name -u username -p password my_command ...`.
