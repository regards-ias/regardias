# Model tools

The `model` group contains commands to delete a model, to export it to a `.xml` file (same format as the one from the GUI) or import the model from a `.xml` file. Use `regardias model --help` for more information.

*Examples:*
* `regardias -e .env_file model delete -m model_name`
* `regardias -e .env_file model export -m model_name`
* `regardias -e .env_file model export --all`
* `regardias -e .env_file model ingest -m model.xml`
