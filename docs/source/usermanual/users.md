# Users tools

The `users` group contains commands to deal with access rights for users. Use `regardias users --help` for more information.

*Examples:*
* `regardias -e .jwst_qualif users add-access-group -m user_email -g group_name`
* `regardias -e .jwst_qualif users remove-access-group -m user_email -g group_name`
* `regardias -e .jwst_qualif users set-access-groups -m user_email -g group1 -g group2 -g group3 ...`
