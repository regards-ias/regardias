# Installation

The recommended method to install `regardias` is to use `pip` (preferably within a virtual environment).

```bash
pip install git+ssh://git@git.ias.u-psud.fr/regards-ias/regardias.git
```

If you want to use the `develop` branch:
```bash
pip install git+ssh://git@git.ias.u-psud.fr/regards-ias/regardias.git@develop
```
