# SIP tools

The `sip` group contains commands to deal with SIPs (see (Regards OAIS implementation)[https://regardsoss.github.io/docs/development/appendices/oais#oais-recommendation]). Use `regardias sip --help` for more information.
You can:
* create a SIP from a template SIP and a FITS file from which the metadata will be retrieved,
* create multiple SIPs at once,
* ingest the SIPs into your Regards instance.

*Examples:*
* `regardias sip generate-from-fits -t sip_template.json -d data.fits -o sip_data.json -p jwst`
* `regardias sip generate-multiple-from-fits -t sip_template.json -i data_list_file.txt -o output_dir -p jwst [--overwrite]`
* `regardias -e .jwst_qualif sip ingest sip_data.json`
* `regardias -e .jwst_qualif sip ingest workdir --from-directory` (All `.json` files will be considered as SIPs.)
* `regardias -e .jwst_qualif sip ingest filelist.txt --from-filelist`
