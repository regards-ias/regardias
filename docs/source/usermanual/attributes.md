# Attributes tools

The `attribute` group contains only the `delete` command for now. You can delete one, several or all attributes for a given project. Use `regardias attribute --help` for more information.

*Examples:*
* `regardias -e .env_file attribute delete -a my_attribute`
* `regardias -e .env_file attribute delete -a some_attribute -a another_attribute`
* `regardias -e .env_file attribute delete --all`
