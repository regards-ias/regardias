# Data tools

The data command groups is meant to prepare the data on your filesystem. In particular, you can create list of data files and move them to the permanent Regards storage place (moving the data product itself plus accompanying data like thumbnails and readme files). Use `regardias data --help` for more information.
