.. Regardias documentation master file, created by
   sphinx-quickstart on Mon Dec 12 10:36:16 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RegardIAS documentation!
============================================

**RegardIAS** is a toolbox project for Regards activities at IAS. It is hosted on `IAS Gitlab <https://git.ias.u-psud.fr/regards-ias/regardias>`_.


.. toctree::
   :maxdepth: 2
   :caption: General

   usermanual/installation.md
   usermanual/quickstart.md
   usermanual/attributes.md
   usermanual/data.md
   usermanual/models.md
   usermanual/projects.md
   usermanual/sip.md
   usermanual/users.md
   usermanual/authors.md

.. toctree::
  :maxdepth: 2
  :caption: API Reference

  api/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
